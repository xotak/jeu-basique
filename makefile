build: *.cpp
	mkdir build
	g++ *.cpp -o build/jeu-linux
	x86_64-w64-mingw32-g++ *.cpp -o build/jeu-win-x64_86.exe -static-libgcc -static-libstdc++
	i686-w64-mingw32-g++ *.cpp -o build/jeu-win-i686.exe -static-libgcc -static-libstdc++