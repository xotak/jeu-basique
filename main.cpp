#include <iostream>
#include <string>
#include "Personnage.hpp"

int main()
{
    std::cout << "Un homme vous a provoque en duel et vous voila maintenant dans l'arene" << std::endl << "Vous n'avez pas le choix de vous battre\n\n";

    Personnage Personnage1("Beargrip", "Epee rouillee", 10), Personnage2("Emberbreaker", "Epee rouillee", 10);

    

    while (Personnage2.estVivant() == true && Personnage1.estVivant() == true) // Si l'adversaire ou le joueur est mort
    {
        std::string action;
        std::cout << "Que voulez-vous faire ? " << std::endl << "Pour attaquer, ecrivez \"attaque\". Pour utiliser une potion, ecrivez \"potion\". Si vous voulez changer d'arme, ecrivez \"arme\"" << std::endl;
        std::cin >> action;

        while (action != "attaque" && action != "potion" && action != "arme") // Validation de l'entree de l'utilisateur
        {
            std::cout << "Action invalide, veuillez reessayer\n";
            std::cin >> action;
        }
        
        if(action == "attaque") {
            std::string typeAttaque;
            std::cout << "Quel type d'attaque voulez vous effectuer ?\nPour une attaque physique, ecrivez \"physique\". Pour une attaque magique, ecrivez \"magique\"" << std::endl;
            std::cin >> typeAttaque;
            while (typeAttaque != "physique" && typeAttaque != "magique")
            {
                std::cout << "Type d'attaque invalide, veuillez reessayer.\n";
                std::cin >> typeAttaque;
            }

            if (typeAttaque == "physique")
            {
                Personnage1.attaquer(Personnage2);
                Personnage2.attaquer(Personnage1);
            }
            else if (typeAttaque == "magique")
            {
                Personnage1.attaqueMagique(Personnage2);
                Personnage2.boirePotionDeMana(50);
            }
            
        }
        else if (action == "potion")
        {
            std::string typePotion;
            std::cout << "Quel type de potion voulez-vous boire ?\nPour une potion de vie, ecrivez \"vie\". Pour une potion de mana, tappez \"mana\"\n";
            std::cin >> typePotion;

            while (typePotion != "vie" && typePotion != "mana")
            {
                std::cout << "Type de potion invalide, veuillez reessayer\n";
                std::cin >> typePotion;
            }

            if (typePotion == "vie")
            {
                Personnage1.boirePotionDeVie(20);
                Personnage2.changerArme("Lame du vide", 15);
            }
            else if (typePotion == "mana")
            {
                Personnage1.boirePotionDeMana(30);
                Personnage2.attaqueMagique(Personnage1);
            }
            
        }
        else if (action == "arme")
        {
            std::string nomArme;
            int degatArme;
            std::cout << "Indiquez le nom de l'arme. Veillez à ne pas mettre d'accents dans le nom de votre arme\n";
            std::cin >> nomArme;
            std::cout << "Indiquez les degats de l'arme\n";
            std::cin >> degatArme;

            while (degatArme < 0 || degatArme > 15)
            {
                std::cout << "Degats de l'arme invalide, veuillez reessayer. (Pour des raisons d'equilibrage, la limite des degats est fixee à 15)\n";
                std::cin >> degatArme;
            }

            Personnage1.changerArme(nomArme, degatArme);
            Personnage2.boirePotionDeVie(20);
            
        }
    
        std::cout << "\n\n=== Status de la bataille ===\n\n";

        Personnage1.afficherEtat();
        Personnage2.afficherEtat();
    }
    
    if (Personnage1.estVivant() == false)
    {
        std::cout << "Vous avez malheureusement perdu. Franchement vous êtes nul, les actions de l'adversaire ne sont même pas aleatiore\n";
    }
    else if (Personnage2.estVivant() == false)
    {
        std::cout << "Felicitations, vous avez montre à votre adversaire qu'il faut pas provoquer en duel n'importe qui. En même temps, c'est très facile de predire ses actions.\n";
    }

    return 0;
}