#ifndef DEF_ARME
#define DEF_ARME

#include <iostream>
#include <string>

class Arme
{
    public:

    Arme();
    Arme(std::string nom, int degat);
    void changer(std::string nouveauNom, int nouveauDegat);
    void afficher() const;
    int getDegat() const;

    private:

    std::string m_nom;
    int m_degat;
};

#endif